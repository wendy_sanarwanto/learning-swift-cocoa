//
//  ViewController.swift
//  HitList
//
//  Created by Wendy Sanarwanto on 1/11/16.
//  Copyright © 2016 Mitrais. All rights reserved.
//

import CoreData
import UIKit

class ViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addName: UIBarButtonItem!
    
    var people = [NSManagedObject]()
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Initialise people records when running the app for the 1st time
        initialise_people();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set a title and register UITableViewCell class with the table view so that when dequeueing a cell, 
        //  the table view will return a cell of the correct type
        title = "\"The List\""
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
    
    // Ensure the table view will have as many rows as the people array has records
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
    // Populate person's name into correct cell
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")
        
        let person = people[indexPath.row]
        
        cell!.textLabel!.text = person.valueForKey("name") as? String
        
        return cell!
    }
    
    
    @IBAction func addName(sender: AnyObject) {
        let add_name_alert = UIAlertController(title: "New name", message: "Add a new Name", preferredStyle: .Alert)
        
        let save_action = UIAlertAction(title: "Save", style: .Default, handler: { (action: UIAlertAction) -> Void in
                let textField = add_name_alert.textFields!.first
                self.save_name(textField!.text!)
                self.tableView.reloadData()
            })
        
        let cancel_action = UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) -> Void in
            
            })
        
        add_name_alert.addTextFieldWithConfigurationHandler { ui_text_field -> Void in
        }
        
        add_name_alert.addAction(save_action)
        add_name_alert.addAction(cancel_action)
        
        presentViewController(add_name_alert, animated: true, completion: nil)
    }
    
    func initialise_people(){
        // 1. Get managed object context
        let app_delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managed_object_context = app_delegate.managedObjectContext
        
        // 2. Create retrieve / fetch query object
        let fetch_request = NSFetchRequest(entityName: "Person")
        
        // 3. Execute fetch object to initialise people collection
        do{
            let results = try managed_object_context.executeFetchRequest(fetch_request)
            people = results as! [NSManagedObject]
        }
        catch let error as NSError {
            print("Could not fetch names: \(error), \(error.userInfo)")
        }
    }
    
    func save_name(name: String){
        // 1. Get managed context's instance
        let app_delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managed_object_context = app_delegate.managedObjectContext
        
        // 2. create a new person record
        let entity_description = NSEntityDescription.entityForName("Person", inManagedObjectContext: managed_object_context)
        let person = NSManagedObject(entity: entity_description!, insertIntoManagedObjectContext: managed_object_context)
        
        // 3. Set person record's name 
        person.setValue(name, forKey: "name")
        
        // 4. Save the created record 
        do {
            try managed_object_context.save()
            people.append(person)
        }
        catch let error as NSError {
            print("Could not save new name: \(error), \(error.userInfo)")
        }
    }
}

