//
//  FavIconTableViewCell.swift
//  OperationQueues
//
//  Created by Wendy Sanarwanto on 1/5/16.
//  Copyright © 2016 Mitrais. All rights reserved.
//

import UIKit

class FavIconTableViewCell: UITableViewCell {
    var operationQueue: NSOperationQueue?
    // The URL that this cell shows.
    var url: NSURL? {
        // When the URL Changes, run this code
        didSet {
            // Create URL Request 
            let request = NSURLRequest(URL: self.url!)
            
            // Display the URL's Host
            self.textLabel?.text = self.url?.host
            
            // Fire off the request, and give it a completion handler plus a queue to run on
            NSURLConnection.sendAsynchronousRequest(request, queue: self.operationQueue!, completionHandler:{ (response, data, error) in
                // the 'data' variable now contains the loaded data;
                // turn it into an image
                if data == nil {return}
                
                let image = UIImage(data: data!)
                
                // Updates to the UI Have to be done on the main queue
                NSOperationQueue.mainQueue().addOperationWithBlock(){
                    // Give the image view the loaded image
                    self.imageView!.image = image
                    
                    // The image view has probably changed size because of the new image, so we need to re-layout the cell.
                    self.setNeedsLayout()
                }
            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }

}
