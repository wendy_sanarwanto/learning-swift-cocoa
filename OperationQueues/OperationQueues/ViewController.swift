//
//  ViewController.swift
//  OperationQueues
//
//  Created by Wendy Sanarwanto on 1/5/16.
//  Copyright © 2016 Mitrais. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let hosts = ["google.com", "apple.com", "secretlab.com.au", "oreilly.com", "twitter.com", "facebook.com"]
    let queue = NSOperationQueue()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hosts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FaviconCell") as! FavIconTableViewCell
        let host = hosts[indexPath.row]
        let url = NSURL(string: "http://\(host)/favicon.ico")
        
        cell.operationQueue = queue
        cell.url = url
        
        return cell
    }
}

