//
//  ViewController.swift
//  activity_indicator_demo
//
//  Created by Wendy Sanarwanto on 1/12/16.
//  Copyright © 2016 Mitrais. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBAction func on_start_clicked(sender: UIButton) {
        activityIndicator.startAnimating()
    }
    
    @IBAction func on_stop_clicked(sender: UIButton) {
        activityIndicator.stopAnimating()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

