//
//  ViewController.swift
//  DynamicSnap
//
//  Created by Wendy Sanarwanto on 1/5/16.
//  Copyright © 2016 Mitrais. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var dynamicAnimator: UIDynamicAnimator?
    var snap:UISnapBehavior!
    
    @IBAction func onTapped(sender: AnyObject) {
        // Getting the tap location
        let tap = sender as! UITapGestureRecognizer
        let point = tap.locationInView(self.view)
        
        // Removing the previous snapping and adding the new one
        if snap != nil {
            self.dynamicAnimator?.removeBehavior(self.snap)
        }
        self.snap = UISnapBehavior(item: self.imageView, snapToPoint:  point)
        self.dynamicAnimator?.addBehavior(self.snap)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.dynamicAnimator = UIDynamicAnimator(referenceView: self.view)
        UIView.animateWithDuration(2) {
            self.view.center = CGPointMake(1,1)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

