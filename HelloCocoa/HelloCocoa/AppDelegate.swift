//
//  AppDelegate.swift
//  HelloCocoa
//
//  Created by Wendy Sanarwanto on 1/4/16.
//  Copyright © 2016 Mitrais. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        application.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        // Doing background task before iOS 7
//        var backgroundTask :UIBackgroundTaskIdentifier! = nil
//        
//        application.beginBackgroundTaskWithExpirationHandler(){
//            application.endBackgroundTask(backgroundTask)
//            backgroundTask = UIBackgroundTaskInvalid
//        }
//        
//        let backgroundQueue = NSOperationQueue()
//        
//        backgroundQueue.addOperationWithBlock(){
//            NSLog("Doing some background work!")
//            
//            application.endBackgroundTask(backgroundTask)
//            backgroundTask = UIBackgroundTaskInvalid
//        }
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(application: UIApplication, performFetchWithCompleteionHandler completionHandler: (UIBackgroundFetchResult) -> Void){
        // we have 30 seconds to do background processing logic
        var error : NSError? = nil
        let data : [String]? = ["{\"name\": \"Chevy\"}"]
        // TODO: Do data fetching
        //let data = downloadSomeData(&error)
        
        // If error happens, call the handler with failing result
        if error != nil {
            completionHandler(UIBackgroundFetchResult.Failed)
            return
        }
        
        if data?.count > 0 {
            completionHandler(UIBackgroundFetchResult.NewData)
            return
        }
        else{
            completionHandler(UIBackgroundFetchResult.NoData)
            return
        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
    }
}

