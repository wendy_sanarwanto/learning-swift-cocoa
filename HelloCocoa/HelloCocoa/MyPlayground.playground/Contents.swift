//: Playground - noun: a place where people can play

import UIKit

// ** Function , loop demo **

func sumNumbers(numbers: Int...) -> Int
{
    var total = 0
    for number in numbers {
        total += number
    }
    return total
}

let sum = sumNumbers(2,5,7,8,10)
print(sum)

// ** Tuple's demo **

let requestTuple = ("request", "{\"destination_name\":\"Raja Ampat\" }")
let destination = requestTuple.1

// ** Array's demo **
var arrayOfIntegers: [Int] = [1,2,3]
let implicitArrayOfIntegers = [1,2,3]

// Empty array
let emptyArrayOfIntegers = [Int]()

// Push value(s) on an existing array
arrayOfIntegers.append(6)

// Insert a value on an existing array
arrayOfIntegers.insert(7, atIndex: 1)

// Remove element at specified index
arrayOfIntegers.removeAtIndex(3)

// Reverse array's elements
arrayOfIntegers.reverse()

// Get the number of array's elements
let elements_count = arrayOfIntegers.count

// ** Dictionary's Demo **

var alpha_team_members = [
    "Commander": "Albert Wesker",
    "Marksman": "Chris Redfield",
    "Fire Support": "Jill Valentine",
    "Scout": "Barry Burton",
    "Medic": "Rebecca Chambers"
]

print(alpha_team_members["Commander"])

// Add a new item into the dictionary
alpha_team_members["Pilot"] = "Brad Vickers"

// Print all dictionary's contents
for member in alpha_team_members {
    print ("==============================")
    print ("Name: \(member.1)")
    print ("Squad Position: \(member.0)")
    print ("============================== \n")
}

// ** Control Flow **

// For in loop - Loop 9 times
var counter = 0
for i in 1 ..< 10 {
    counter = i
}
print("counter = \(counter)")

// If let - check if the optional variabel has value or not. If it has a value, assign the value to a constant value then run some code
let optional_something: String? = "A String value"
if let theString = optional_something {
    print("theString = \"\(theString)\"")
}
else{
    print("theString is nil")
}

// ** Switches **

// switch against an integer value
let some_int_constant = 3
switch some_int_constant{
    case 1: print("It's 1")
    case 2: print("It's 2")
    case 3: print("It's 3")
    default: break
}

// switch against an string value
let some_string_constant = "Hello"
switch some_int_constant {
    case some_int_constant: print("A Greeting")
    case some_int_constant: print("A Farewell")
    default: print("Something else")
}

// Switch against a tuple
let a_tuple_constant = ("Name", "Wendy Sanarwanto")

switch a_tuple_constant{
    case ("Name", "Wendy Sanarwanto"): print("Hello, Wendy Sanarwanto")
    default: print("Hello Stranger")
}

// Switch against a range of numbers
switch some_int_constant{
    case 1...10: print("Number is between 1 to 10")
    case 11...20: print("Number is between 11 to 20")
    default: print("Number is something else")
}

// ** Function **

// Function return tuple
func tupleFunc (firstValue: Int, secondValue: Int) -> (doubled: Int, quadrupled: Int){
    return (firstValue * 2, secondValue * 4)
}

tupleFunc(3, secondValue: 6)
tupleFunc(3, secondValue: 6).doubled
tupleFunc(3, secondValue: 6).quadrupled

// Function with passing by reference parameter ( inout)

func add_numbers(num_1: Int, num_2: Int, inout result: Int){
    result = num_1+num_2
}

var add_nums_result = 0
add_numbers(5, num_2: 10, result: &add_nums_result)

// ** Function as variable **
func multiply(num_1: Int, num_2: Int) -> Int {
    return num_1 * num_2
}
var my_func: (Int, Int) -> Int;
my_func = multiply

let five_by_3 = my_func(5, 3)

// Function capture a value and use it multiple times
func createIncrementor (incrementAmount: Int) -> () -> Int {
    var amount = 0
    func incrementor() -> Int {
        amount += incrementAmount
        return amount
    }
    
    return incrementor
}

var incrementByTen = createIncrementor(10)
incrementByTen()
incrementByTen()

// ** Closure **

var numbers = [2,5,98,2,13]
var sortedNumbers = numbers.sort({(n1: Int, n2: Int) -> Bool in
                        return n2 > n1
                    })

// Closure in Less verbose 
sortedNumbers = numbers.sort({n1, n2 in
    return n2 > n1
})

// Closure which contain single line of code only
sortedNumbers = numbers.sort({ $1 > $0 })

// Store closure on variable
var comparator = {(n1: Int, n2: Int) in n2 > n1}
sortedNumbers = numbers.sort(comparator)

// ** Objects, Initialisation & Deinitialisation **

class Product {
    var name: String?
    var product_price: Int = 0
    
    init(){
        print("Product is created")
    }
    
    convenience init(name: String?, price: Int){
        self.init()
        self.name = name
        self.product_price = price
    }

    deinit {
        print("Product is deinitialised")
    }
    
    func description() -> String {
        return "\(name)"
    }
    
    func price() -> Int {
        return product_price
    }
}

var ssd = Product(name: "SSD Corsair Neutron GTX 480 GB", price: 200)
print( "Product's description: \(ssd.description())")
print("Price: \(ssd.price())")

// ** Inheritance, property observer ( willSet, didSet), computed getter, lazy property **

class SSD : Product {
    // Property observer
    var capacity: Int = 0 {
        willSet(newNumber) { print("New capacity: \(newNumber)") }
        didSet(oldNumber) { print("Prior capacity: \(oldNumber)") }
    }
    var width: Int = 0
    lazy var height: Int = 0
    // Computed getter
    var dimension : Int {
        get {
            return width * height
        }
    }
    
    convenience init(name: String?, price: Int, capacity: Int){
        self.init(name: name, price: price)
        self.capacity = capacity
    }
    
    func disk_capacity() -> String{
        return "Disk capacity: \(capacity) GB"
    }
}

var ssd_neutron_gtx = SSD(name: "SSD Corsair Neutron GTX 240 GB", price: 130, capacity: 240)
ssd_neutron_gtx.description()
ssd_neutron_gtx.disk_capacity()
ssd_neutron_gtx.price()

// ** Protocol **

protocol Blinking {
    var isBlinking : Bool { get }
    var blinkSpeed : Double { get set }
    
    func startBlinking(blinkSpeed: Double) -> Void
}

public class Light: Blinking {
    var isBlinking: Bool = false
    var blinkSpeed: Double = 0.0
    
    public func startBlinking(blinkSpeed: Double) {
        isBlinking = true
        self.blinkSpeed = blinkSpeed
    }
}

var blinking_thing: Blinking? = Light()
blinking_thing?.startBlinking(4.0)
blinking_thing?.blinkSpeed

// ** Extensions **

extension Int {
    var doubled: Int {
        return self * 2
    }
    
    func multiplyWith(anotherNumber: Int) -> Int {
        return self * anotherNumber
    }
}

2.doubled
4.multiplyWith(10)

// ** Operators **

public class Vector2D {
    var x: Float = 0.0
    var y: Float = 0.0
    
    init(x: Float, y: Float){
        self.x = x
        self.y = y
    }

}

func +(left : Vector2D, right : Vector2D) -> Vector2D {
    let result = Vector2D(x: left.x + right.x, y: left.y + right.y)
    return result
}

let point_a = Vector2D(x: 5.5, y: 7.3)
let point_b = Vector2D(x: -3.4, y: 11.1)

var point_c = point_a + point_b
point_c.x
point_c.y

// ** Generics **

public class Tree<T> {
    var value: T
    var children: [Tree<T>] = []
    
    init(value : T){
        self.value = value
    }
    
    func addChild(value: T) -> Tree<T>{
        let newChild = Tree<T>(value: value)
        children.append(newChild)
        return newChild
    }
    
}

// Tree of integers
var integerTree = Tree<Int>(value: 5)

integerTree.addChild(8)
integerTree.addChild(13)

// Tree of Strings
var stringsTree = Tree<String>(value: "Hello")

stringsTree.addChild("Mr.")
stringsTree.addChild("Sanarwanto")

// ** Strings **

// Assign empty string
let emptyString = ""
let yetAnotherEmptyString = String()

// Checking a stirng is empty
emptyString.isEmpty

// Composing strings
var composingString = "Hello"
composingString += ", World!" // = "Hello, World!"

// Comparing strings
let string_a: String = "Hello"
let string_b: String = "Hel" + "lo"

if string_a == string_b {
    print("The strings are equal")
}

// Check if 2 string variabel points to a same string reference

if string_a as AnyObject === string_b as AnyObject {
    print("The strings are the same object")
}
else {
    print("The strings are having different objects")
}

// Has prefix , has suffix
if string_a.hasPrefix("Hel"){
    print("string_a has prefix 'Hel'")
}

if string_b.hasSuffix("lo"){
    print("string_a has prefix 'lo'")
}

// ** Data **

// Convert string as NSData
let datafied_string_a = string_a.dataUsingEncoding(NSUTF8StringEncoding)

// Loading Data from files and URLs
if let fileUrl = NSBundle.mainBundle().URLForResource("SomeFile", withExtension: "json"){
    let loadedDataFromUrl = NSData(contentsOfURL: fileUrl)
}

// Loading Data from Path
if let filePath = NSBundle.mainBundle().pathForResource("SomeFile", ofType: "json"){
    let loadedDataFromPath = NSData(contentsOfFile: filePath)
}

// ** Serialisation & Deserialisation **
class SerializableData: NSObject, NSCoding {
    var some_data :String?
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(some_data, forKey: "Data")
    }
    
    override init(){
        some_data = "My Data"
    }
    
    required init?(coder aDecoder: NSCoder) {
        some_data = aDecoder.decodeObjectForKey("Data") as? String
    }
}

let aData = SerializableData()

aData.some_data = "{name: 'Billy Sarmanto', address: 'Durian st. 50, BNE, NSW'}"

let objectConvertedData = NSKeyedArchiver.archivedDataWithRootObject(aData)

let reloadedData = NSKeyedUnarchiver.unarchiveObjectWithData(objectConvertedData)

reloadedData?.some_data

// ** Delegation **
// Define delegate's protocol 
protocol SignedInDelegate {
    func handleSignedInEvent()
}

class AuthController {
    var signInDelegate : SignedInDelegate?
    
    func signInSucceded(){
        signInDelegate?.handleSignedInEvent()
    }
}

class SignInHandler : SignedInDelegate{
    func handleSignedInEvent(){
        print("A user has been signed in successfully")
    }
}

let auth_ctl = AuthController()
auth_ctl.signInDelegate = SignInHandler()
auth_ctl.signInSucceded()