//
//  ViewController.swift
//  HelloCocoa
//
//  Created by Wendy Sanarwanto on 1/4/16.
//  Copyright © 2016 Mitrais. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var greetButton: UIButton!
    @IBAction func showAlert(sender: AnyObject) {
        // Create UIALertController instance
        let alertController = UIAlertController(title:"We greet", message: "Hello World !", preferredStyle: UIAlertControllerStyle.Alert)
        
        // Add close action
        alertController.addAction(UIAlertAction(title:"Close", style: UIAlertActionStyle.Default, handler: nil))
        
        // Show the alert dialog
        self.presentViewController(alertController, animated: true, completion: nil)
        
        // Update the button's title to Clicked
        self.greetButton.setTitle("Clicked", forState: UIControlState.Normal)
        
        // Send custom notification
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.postNotificationName( "Greet", object: "Hello World")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

